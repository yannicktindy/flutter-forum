import 'package:flutter/material.dart';
import 'package:forum/providers/auth_provider.dart';
import 'package:forum/widgets/myscaffold.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  Map<String, String?> _userInfo = {};

  @override
  void initState() {
    super.initState();
    _loadUserInfo();
  }

  Future<void> _loadUserInfo() async {
    // final authProvider = Provider.of<AuthProvider>(context, listen: false);
    // _userInfo = await authProvider.getUserInfo();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      name: 'Profile',
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Bonjour, bienvenue sur votre profil !',
              style: const TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            
          ],
        ),
      ),
    );
  }
}