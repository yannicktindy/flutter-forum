import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/auth_provider.dart';
import '../utils/secure_storage.dart' show SecureStorage;

class MyScaffold extends StatefulWidget {
  final Widget body;
  final String name;
  final List<Widget> actions;


  const MyScaffold({
    Key? key,
    required this.body,
    required this.name,
    this.actions = const [],
  }) : super(key: key);

  @override
  _MyScaffoldState createState() => _MyScaffoldState();
}

class _MyScaffoldState extends State<MyScaffold> {
  final SecureStorage _secureStorage = SecureStorage();
  Map<String, String?> _userInfo = {};

  @override
  void initState() {
    super.initState();
    _loadUserInfo();
  }

  Future<void> _loadUserInfo() async {
    final secureStorage = SecureStorage();
    _userInfo = await secureStorage.readUserInfo();
    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);

    return Scaffold(
      appBar: AppBar(
        title: authProvider.isLoggedIn
            ? Text('${_userInfo['firstName']} ${_userInfo['lastName']}', style: const TextStyle(color: Colors.white))
            : Text(widget.name, style: const TextStyle(color: Colors.white)),
        elevation: 10.0,
        centerTitle: true,
        actions: <Widget>[
          if (!authProvider.isLoggedIn)
            IconButton(
              icon: const Icon(Icons.app_registration, color: Colors.white),
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  '/register',
                );
              },
            ),
          IconButton(
            icon: Icon(
              authProvider.isLoggedIn ? Icons.login : Icons.logout,
              color: Colors.white,
            ),
            onPressed: () {
              if (authProvider.isLoggedIn) {
                authProvider.logout();
                Navigator.pushNamedAndRemoveUntil(
                  context,
                  '/profile',
                  (Route<dynamic> route) => false,
                );
              } else {
                Navigator.pushNamed(
                  context,
                  '/login',
                );
              }
            },
          ),
        ],
      ),
      backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
      body: widget.body,
      floatingActionButton: FloatingActionButton(
        onPressed: null,
        child: const Icon(Icons.add),
      ),
    );
  }
}