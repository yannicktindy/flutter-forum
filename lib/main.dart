import 'package:flutter/material.dart';
import 'package:forum/screens/home_screen.dart';
import 'package:forum/screens/register_screen.dart';
import 'package:forum/screens/login_screen.dart';
import 'package:forum/screens/profile_screen.dart';
import 'package:provider/provider.dart';
import 'providers/auth_provider.dart';
// import 'package:forum/screens/messages_screen.dart';

void main() {
  runApp(
    ChangeNotifierProvider(
      create: (context) => AuthProvider(),
      child: MainApp(),
    ),
  );
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter',
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          backgroundColor: Colors.indigo,
        ),
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (context) => const HomeScreen(),
        '/register': (context) => const RegisterScreen(),
        '/login': (context) => const LoginScreen(),
        '/profile': (context) => const ProfileScreen(),
        // '/messages': (context) => const MessagesScreen(),
      },
    );
  }
}

